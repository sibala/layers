import * as d3 from "d3";
import {makeLoihi, makeRouter} from "./LoihiVis.js";
 
import { Dropdown, DropdownButton } from 'react-bootstrap';
import React, { useEffect, useRef, useState } from 'react';
import grapher from './view-grapher';
import clone from "clone";

function App() {

  // 'data' is the sample data that will be passed into a D3 
  const [data, updateData] = useState({
    "latency": {
      name: "latency",
      children: 
          [
          {'core1': {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}},
          {'core2': {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}},
          {'core3': {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}},
          {'core4': {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}}
        ]
      },
    "foo": {
      name: "foo",
      children: 
          [
          {'core1': {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}},
          {'core2': {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}},
          {'core3': {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}},
          {'core4': {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}},
          {'core1': {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}},
          {'core2': {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}},
          {'core3': {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}},
          {'core4': {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}}
        ]
      },
    "bar": {
      name: "foo",
      children: 
          [
          {name: 'foo1', children: [{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}, {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]} ]},
          {name: 'foo1', children: [{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}, {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]} ]},
          {name: 'foo1', children: [{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}, {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]} ]},
          {name: 'foo1', children: [{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}, {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]} ]},
          {name: 'foo1', children: [{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}, {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]} ]},
          {name: 'foo1', children: [{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}, {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]} ]},
          {name: 'foo1', children: [{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}, {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]} ]},
          {name: 'foo1', children: [{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}, {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]} ]},
          {name: 'foo1', children: [{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}, {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]} ]},
          {name: 'foo1', children: [{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}, {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]} ]},
          {name: 'foo1', children: [{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}, {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]} ]},
          {name: 'foo1', children: [{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]}, {'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]},{'percent': 14, 'layer': ['FC1000', 'Conv 5x5'], 'Neurons': [890, 25]} ]},

        ]
      },
      

  });

  const graphRef = useRef(null);

  const [dropdownData, updateDropdownData] = useState(['Core View', 'Network View'])
  const [dropdownTitle, updateDropdownTitle] = useState('Select')
  const [detailsPane, updateDetailsPane] = useState(<span>Select a Core</span>);
  const coreDataRef = useRef({});
  const coreIDRef = useRef({});
  fetch("tmp_hw_data.json")
      .then(response => response.json())
      .then(data => {coreDataRef.current = data.core_data;});   
  var nRouterRow = 3
  var nRouterCol = 3
  const routerData = new Array(nRouterRow).fill(0).map(() => new Array(nRouterCol).fill(randPrec()));
  const coreIDs = new Array(nRouterRow).fill(0).map((k, i) => 
                                new Array(nRouterCol).fill(0).map( (q, j)=>[i*nRouterCol*4 + j*4 + 1,
                                                                            i*nRouterCol*4 + j*4 + 2,
                                                                            i*nRouterCol*4 + j*4 + 3,
                                                                            i*nRouterCol*4 + j*4 + 4, ])
                                                                            );
  coreIDRef.current = coreIDs

  const itemSelected = (e) => {
    updateDropdownTitle(e.target.textContent)

    // use D3 to visualize data
    console.log('dropdown: ', e.target.textContent)
    if (e.target.textContent == "energy usage"){
      renderGrapher()
    }
    else{
      renderD3(e.target.textContent)
    }
    
  }

  const getItems = () => {
    
    return dropdownData.map((value, i) => {
      // the key property is required. placing a unique identifier such as the index of the list will do
      return  <Dropdown.Item key={i}  onClick={(e) => itemSelected(e)} >{value}</Dropdown.Item>
    })

  }

  function randPrec(max) {
    return  "" 
    // Math.floor(Math.random() * 100).toString() + "%";
  }

  const renderGrapher = () =>{
    var element = document.getElementById('D3-map') 
    element.innerHTML = "";
    const isCompound = false;
    const options = {};
    var graph = new grapher.Graph(isCompound, options);
    graph.build(document, element)
  }

  const handleClick = (nodeObjID, coreID, valid) => {
    // this function handles click action of each node.
    const coreData = coreDataRef.current[coreID]
    console.log("clicked", coreData);
    updateDetailsPane(
      <div id='scroll'>
        <div className='grey '>
          <div className="meta">Core: </div> 
          <div className="meta-right"><strong>{coreID}</strong></div>
        </div>
        <div>
          <div className="meta">Utilization: </div> 
          <div className="meta-right"><strong>{coreData.utilization.toFixed(2)}%</strong></div>
        </div>
        <div className='grey'>
          <div className="meta">Spikes In / step: </div> 
          <div className="meta-right"><strong>{coreData.spikes_in_per_timestep.toFixed(0)}</strong></div>
        </div>
        <div>
          <div className="meta">Spikes out / step: </div> 
          <div className="meta-right"><strong>{coreData.spikes_out_per_timestep.toFixed(0)}</strong></div>
        </div>

        <div className='grey'>
          <div className="meta">Mem Used:  </div> 
          <div className="meta-right"><strong>{coreData.mem_used.toFixed(2)} kB</strong></div>
        </div>

        <div>
          <div className="meta">#Neurons: </div> 
          <div className="meta-right"><strong>{coreData.n_neurons}</strong> </div>
        </div>

        <div className='grey'> 
          <div className="meta">#Weights:</div> 
          <div className="meta-right"><strong>{coreData.n_weights}</strong></div>
        </div>


        <div>Spikes In: </div>
           <img className='details' src={coreData.layers_in_spike_plot}></img>
           <div className='details_grey'>Spikes Out: </div>
          <img className='details' src={coreData.layers_out_spike_plot}></img>
          <div className='details_grey'>Layers: </div>
           <img className='details'src={coreData.layers_pie_plot}></img>

      </div>
    )
  };

  const renderD3 = (dropDownValue) => {

    var width = 500;
    var height = 500;
    console.log('width :', window.innerWidth)
    //Create SVG element
    
    var svg = d3.create("svg")
      .attr("width", width)
      .attr("height", height)

    if (dropDownValue === 'Core View'){
      var nRouterRow = 3
      var nRouterCol = 3
      const coreIDs = coreIDRef.current
      var loihi = makeLoihi(svg, width, height, nRouterRow, nRouterCol, coreIDs, routerData, handleClick)
      var element = document.getElementById('D3-map') 
      element.innerHTML = '';
      element.appendChild(svg.node())
    } else{
      var loihi = makeLoihi(svg, width, height, nRouterRow, nRouterCol, coreIDs, routerData, handleClick)
      var element = document.getElementById('D3-map') 
      element.innerHTML = '';
      element.appendChild(svg.node())
    }

  }

  return (
    
    <div style={{padding: 50}} vertical-align = "top" >
      <div style={{margin: '0 auto', display: 'inline-block'}} vertical-align = "top" > 
        <DropdownButton id="dropdown-basic-button" title={dropdownTitle} >
          {getItems()}
        </DropdownButton>
        <div id="D3-map">
                {/* render D3 data here */}
          </div>
      </div>
      <div style={{ display: 'inline-block', verticalAlign: 'top'}}>
              {detailsPane}
      </div>
    </div>
  );
}

export default App;
