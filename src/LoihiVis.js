import * as d3 from "d3";



export function makeLoihi(svg, width, height, nRouterRow, nRouterCol, coreIDs, routerData, handleClick){
    var boarder = 30
    var routerWidth = Math.min((width-2*boarder) / nRouterCol, (height-2*boarder) / nRouterRow)
    var spacing = routerWidth * .1

    let loihiGroup = svg.append('g')
    for (let xInd = 0; xInd < nRouterCol; xInd++){
        for (let yInd = 0; yInd < nRouterRow; yInd++){
            var rg = makeRouter(loihiGroup, routerWidth, coreIDs[yInd][xInd], routerData[yInd][xInd], handleClick)
            var x = xInd * routerWidth
            var y = yInd * routerWidth
            rg.attr('transform',`translate(${x}, ${y})`)
        }
    }
    loihiGroup.attr('transform', `translate(${boarder},${boarder})`)
    return svg
}

export function makeRouter(svg, width, coreIDs, routerData, handleClick){
    let routerGroup = svg.append('g')
    var fontSize = 12;
    //initializing
    let boarder = width * 0.1
    let spacing = (width-boarder*2) * 0.15
    let coreWidth = (width-boarder*2)/2 - spacing

    var corexs = [0, width/2+spacing, 0, width/2+spacing]
    var coreys = [0, 0, width/2+spacing, width/2+spacing]
    var linex1 = [0, 0, width/2 - boarder/2, 0]
    var linex2 = [width-boarder, width-boarder, width/2 - boarder/2, width]
    var liney1 = [0, width-boarder, 0, width/2 - boarder/2]
    var liney2 = [width-boarder, 0, width, width/2 - boarder/2]

    //connecting lines
    for (var i=0; i<4; i++){
        routerGroup.append('line')
            .attr('x1', linex1[i]).attr('x2', linex2[i])
            .attr('y1', liney1[i]).attr('y2', liney2[i])
            .attr('stroke','#505050').attr('stroke-width',2)
    }

    var cores = routerGroup.selectAll("g")
        .data(coreIDs)
        .enter().append("g")
        .attr("transform", function(d, i) { return `translate(${corexs[i]},${coreys[i]})`; })
        .on('click', handleClick);

    cores.append("rect")
        .attr("width", coreWidth)
        .attr("height", coreWidth)
        .attr("key", coreIDs)
        .attr("fill", "white")
        .attr('fill', '#eaec13').attr('stroke','#505050').attr('stroke-width',2);
        // .on('click', handleClick);
        

    cores.append("text")
        .attr("dy", coreWidth/2 + fontSize/2)
        .attr("dx", coreWidth/2 )
        .attr("font-family", "Arial Black")
        .attr("fill", "black")
        .attr('font-size', fontSize)
        .style("text-anchor", "middle")
        .text(function(d) { return d; });
        // .on('click', handleClick);
        
    routerGroup.append('circle')
        .attr('cx',width/2-boarder/2).attr('cy', width/2-boarder/2)
        .attr('r', spacing)
        .attr('fill', '#6e6e6e').attr('stroke','#505050').attr('stroke-width',2)
    routerGroup.append('text')
        .attr('x', width/2-boarder/2)
        .attr('y', width/2 - fontSize/2)
        .text(routerData)
        .attr("font-family", "Arial Black")
        .attr("fill", "black")
        .attr('font-size', fontSize)
        .style("text-anchor", "middle")

  routerGroup.attr('transform',`translate(${boarder}, ${boarder})`)
    return routerGroup
}
// export function makeRouter(svg, width){
//     let boarder = width * 0.1
//     let spacing = (width-boarder*2) * 0.15
//     let coreWidth = (width-boarder*2)/2 - spacing
//     let routerGroup = svg.append('g')
//     var textData = ['1','2','3','4']
//     //connecting lines
//     routerGroup.append('line')
//         .attr('x1', 0).attr('x2', width-boarder)
//         .attr('y1', 0).attr('y2', width-boarder)
//         .attr('stroke','#505050').attr('stroke-width',2)
//     routerGroup.append('line')
//         .attr('x1', 0).attr('x2',  width-boarder)
//         .attr('y1', width-boarder).attr('y2', 0)
//         .attr('stroke','#505050').attr('stroke-width',2)
//     routerGroup.append('line')
//         .attr('x1', width/2 - boarder/2).attr('x2',  width/2 - boarder/2)
//         .attr('y1', 0).attr('y2', width)
//         .attr('stroke','#505050').attr('stroke-width',2)
//     routerGroup.append('line')
//         .attr('x1', 0).attr('x2',  width)
//         .attr('y1',  width/2 - boarder/2).attr('y2',  width/2 - boarder/2)
//         .attr('stroke','#505050').attr('stroke-width',2)
//     // top left
//     routerGroup.append('rect')
//         .attr('x',0).attr('y',0)
//         .attr('height', coreWidth).attr('width', coreWidth)
//         .attr('fill', '#eaec13').attr('stroke','#505050').attr('stroke-width',2)
//     // top right
//     routerGroup.append('rect')
//         .attr('x',width/2+spacing).attr('y',0)
//         .attr('height', coreWidth).attr('width', coreWidth)
//         .attr('fill', '#eaec13').attr('stroke','#505050').attr('stroke-width',2)
//     // bottom left
//         routerGroup.append('rect')
//         .attr('x',0).attr('y', width/2+spacing)
//         .attr('height', coreWidth).attr('width', coreWidth)
//         .attr('fill', '#eaec13').attr('stroke','#505050').attr('stroke-width',2)
//     // bottom right
//     routerGroup.append('rect')
//         .attr('x',width/2+spacing).attr('y', width/2+spacing)
//         .attr('height', coreWidth).attr('width', coreWidth)
//         .attr('fill', '#eaec13').attr('stroke','#505050').attr('stroke-width',2)
//     // router
//     routerGroup.append('circle')
//         .attr('cx',width/2-boarder/2).attr('cy', width/2-boarder/2)
//         .attr('r', spacing)
//         .attr('fill', '#6e6e6e').attr('stroke','#505050').attr('stroke-width',2)

//     var addText = routerGroup.selectAll('text')
//         .data(textData).enter().append('text');

//     var textElements = addText
//         .attr('x', function(d, i){return 10* i })
//         .attr('y', function(d,i){return 10* i})
//         .attr("font-family", "Arial Black")
//         .attr("font-size", "20px")
//         .attr("fill", "black")
//         .text(function(d, i){return d;});

//     routerGroup.attr('transform',`translate(${boarder}, ${boarder})`)
//     return routerGroup
// }
