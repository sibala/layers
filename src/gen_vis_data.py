import numpy as np
import json
import os
import matplotlib.pyplot as plt
import matplotlib
import tensorflow as tf

core_color = 'y'
core_shape = 'rect'

UI_HW_Component = {"x": 0, "y": 0, "w":50, "h": 50,
                "shape":  'rect', "color": 'y', "type":  'core', 'id': 0,
                "total_mem": 512}

#place the core and router data in the loihi chip format
def place_objects_loihi(core_data_list, router_data_list, route_connections, width: int= 500, height: int=500,
                        n_router_rows: int=3, n_router_cols: int=3):
    router_group_width = np.min([width / n_router_cols, height / n_router_rows])
    BOARDER = 30
    router_group_width = np.min([(width-2*BOARDER) / n_router_cols, (height-2*BOARDER) / n_router_rows])
    spacing = router_group_width * .1
    #place the routers
    router_ind = 0
    for router_row in range(n_router_rows):
        for router_col in range(n_router_cols):
            router_data_list[router_ind]['x'] = router_group_width * router_col + router_group_width //2 
            router_data_list[router_ind]['y'] = router_group_width * router_row + router_group_width //2 

            router_ind += 1
 
    router_row_ind = 0 # runs from 0 to 2 corresponds to the router row
    router_col_ind = 0 # runs from 0 to 2 corresponds to the router column
    for core_data in core_data_list:
        pass
    #place the cores
    router_row_ind = 0 # runs from 0 to 2 corresponds to the router row
    router_col_ind = 0 # runs from 0 to 2 corresponds to the router column
    router_core_ind  = 0 # runs from zero to 3 corresponds to the position of the core in the router group
    for core_data in core_data_list:
        pass

#placeholder function 
def gen_data_hw(n_cores=36, n_routers=9):
    matplotlib.rcParams.update({'font.size': 26})
    if not os.path.exists('./tmp_fig/'):
            os.mkdir('./tmp_fig')
    core_data_list = []
    for core_id in range(n_cores):
        d = UI_HW_Component.copy()
        d['utilization'] = np.random.rand()
        d['spikes_in_per_timestep'] = np.max([np.random.normal()*1000+1000, 0])
        d['spikes_out_per_timestep'] = np.max([np.random.normal()*1000+1000, 0])
        d['mem_used'] = np.round(np.random.rand()*d['total_mem'])
        d['n_neurons'] = np.round(np.random.rand()*500)
        d['n_weights'] = d['n_neurons']*52
        d['id'] = core_id
        
        #plot layers pie chart
        d['layers'] = {'Conv1':256, 'Conv2':128, 'FC2':41}
        fig, ax = plt.subplots()
        ax.pie(d['layers'].values(),  labels=d['layers'].keys(), autopct='%1.1f%%',
                shadow=True, startangle=90)
        plt.tight_layout()
        plt.savefig(f"./tmp_fig/Core{core_id}_loihi_layers.png")
        d['layers_pie_plot'] = f"./tmp_fig/Core{core_id}_loihi_layers.png"
        plt.close(fig)

        #plot spike histogram
        mean = np.random.rand() * 200 + 200
        in_spikes = np.random.normal(loc=mean, scale=100, size=(300,))
        fig, ax = plt.subplots()
        bins = np.arange(0,500, 20)
        ax.hist(in_spikes, bins = bins)
        ax.set_xlabel('Incomming Spikes')
        ax.set_ylabel('Samples')
        ax.set_xlim([0, 500])
        plt.tight_layout()
        plt.savefig(f"./tmp_fig/Core{core_id}_spikes_in_hist.png")
        d['layers_in_spike_plot'] = f"./tmp_fig/Core{core_id}_spikes_in_hist.png"
        plt.close(fig)
        
        #plot spike histogram
        mean = np.random.rand() * 200 + 200
        out_spikes = np.random.normal(loc=mean, scale=100, size=(300,))
        fig, ax = plt.subplots()
        ax.hist(out_spikes, bins = bins)
        ax.set_xlabel('Outgoing Spikes')
        ax.set_ylabel('Samples')
        plt.tight_layout()
        plt.savefig(f"./tmp_fig/Core{core_id}_spikes_out_hist.png")
        d['layers_out_spike_plot'] = f"./tmp_fig/Core{core_id}_spikes_out_hist.png"
        plt.close(fig)
        
        core_data_list.append(d)
    #reshaping the list so that it's of the form (nCoreGroupRows, nCoreGroupCols, core)
    #so that it's nicely usable by the vis module
    #The reshaping is no longer helpful
    # n_core_group_rows = np.sqrt(n_cores/4).astype(int)
    # n_core_group_cols = np.sqrt(n_cores/4).astype(int)
    # core_data = []
    # for i in range(n_core_group_rows):
    #     core_data.append([])
    #     for j in range(n_core_group_cols):
    #         core_data[i].append([])
    #         for k in range(4):
    #             core_data[i][j] = core_data_list[i*n_core_group_cols + j*4 : i*n_core_group_cols + j*4+4]

    #router data not working at the moment
    router_data_list = []
    for router_id in range(n_cores, n_cores + n_routers):
        d = UI_HW_Component.copy()
        d['type'] = 'router'
        d['utilization'] = np.random.rand()
        d['spikes_per_timestep'] = np.max([np.random.normal()*1000+1000, 0])
        d['id'] = core_id
        core_data_list.append(d)

    route_connections = [[(router_id, core_id) for core_id in range((router_id-n_cores)*4, (router_id-n_cores)*4+4)]
                        for router_id in range(n_cores, n_cores + n_routers)]

    return core_data_list, router_data_list, route_connections

def gen_hw_json(core_data_list, router_data_list):
    json_dict = {'core_data': core_data_list,
                'router_data': router_data_list}
    json.dump(json_dict, open('../public/tmp_hw_data.json', 'w'))

#placeholder function 
def gen_data_net(model, sim_data):
    matplotlib.rcParams.update({'font.size': 26})
    layer_data_list = []
    for layer in model.layers:
        if layer.name in sim_data['activations'].keys():
            spike_rate = sim_data['spike_rates'][layer.name]
            activation = sim_data['activations'][layer.name]
        else:
            continue
        layer_data = {}
        layer_data['name'] = layer.name
        layer_data['input_shape'] = layer.input_shape
        layer_data['output_shape'] = layer.output_shape

        #plot core pie chart
        total_neurons = np.prod(layer.output_shape[1:])
        layer_data['cores'] = {0:256,}
        fig, ax = plt.subplots()
        ax.pie(layer_data['cores'].values(),  labels=layer_data['cores'].keys(), autopct='%1.1f%%',
                shadow=True, startangle=90)
        plt.tight_layout()
        plt.savefig(f"./tmp_fig/{layer.name}_loihi_layers.png")
        layer_data['core_pie_plot'] = f"./tmp_fig/{layer.name}_core_pie.png"
        
        #plot spike histogram
        mean = np.random.rand() * 200 + 200
        in_spikes = np.random.normal(loc=mean, scale=100, size=(300,))
        fig, ax = plt.subplots()
        bins = np.arange(0,500, 20)
        ax.hist(in_spikes, bins = bins)
        ax.set_xlabel('Incomming Spikes')
        ax.set_ylabel('Samples')
        ax.set_xlim([0, 500])
        plt.tight_layout()
        plt.savefig(f"./tmp_fig/{layer.name}_spikes_in_hist.png")
        layer_data['in_spike_hist_plot'] = f"./tmp_fig/{layer.name}_spikes_in_hist.png"
        plt.close(fig)

        #plot spike histogram
        mean = np.random.rand() * 200 + 200
        out_spikes = np.random.normal(loc=mean, scale=100, size=(300,))
        fig, ax = plt.subplots()
        ax.hist(out_spikes, bins = bins)
        ax.set_xlabel('Outgoing Spikes')
        ax.set_ylabel('Samples')
        plt.tight_layout()
        plt.savefig(f"./tmp_fig/{layer.name}_spikes_out_hist.png")
        layer_data['out_spike_hist_plot'] = f"./tmp_fig/{layer.name}_spikes_out_hist.png"
        plt.close(fig)

        #plot activations
        fig, ax = plt.subplots()
        ax.hist(activation.reshape(-1), bins = np.linspace(0,1,20))
        ax.set_xlabel('Activation')
        ax.set_ylabel('Neurons')
        plt.tight_layout()
        plt.savefig(f"./tmp_fig/{layer.name}_activations.png")
        layer_data['activations_hist_plot'] = f"./tmp_fig/{layer.name}_activations.png"
        plt.close(fig)

        #plot spike Rates
        fig, ax = plt.subplots()
        ax.hist(spike_rate.reshape(-1), bins = np.linspace(0,1,20))
        ax.set_xlabel('Spike Rate')
        ax.set_ylabel('Neurons')
        plt.tight_layout()
        plt.savefig(f"./tmp_fig/{layer.name}_activations.png")
        layer_data['spike_rate_hist_plot'] = f"./tmp_fig/{layer.name}_spike_rates.png"
        plt.close(fig)

        #plot ANN SNN Correlation
        fig, ax = plt.subplots()
        ax.scatter(activation.reshape(-1), spike_rate.reshape(-1), s = 1)
        ax.set_xlabel('ANN Activations')
        ax.set_ylabel('SNN Spike Rates')
        plt.tight_layout()
        plt.savefig(f"./tmp_fig/{layer.name}_ANN_SNN_correlation.png")
        layer_data['ann_snn_correlation'] = f"./tmp_fig/{layer.name}_ANN_SNN_correlation.png"
        plt.close(fig)

        layer_data_list.append(layer_data)
    return layer_data_list

if __name__ == '__main__':
    core_data_list, router_data_list, route_connections = gen_data_hw()
    gen_hw_json(core_data_list, router_data_list)
    model = tf.keras.Sequential([   
        tf.keras.layers.Conv2D(8, (3,3), activation='relu', input_shape=(32, 32, 3), name='Conv1'),
        tf.keras.layers.Conv2D(16, (3,3), activation='relu', name='Conv2'),
        tf.keras.layers.Conv2D(32, (3,3), activation='relu', name='Conv3'),
        tf.keras.layers.Conv2D(64, (3,3), activation='relu', name='Conv4'),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(1024, name='FC1'),
        tf.keras.layers.Dense(10, name='FC2')])
    activations = {'Conv1':np.random.rand(31,31,8),
                                'Conv2':np.random.rand(30,30,16),
                                'Conv3':np.random.rand(29,29,32),
                                'Conv4':np.random.rand(28,28,64),
                                'FC1':np.random.rand(1024),
                                'FC2':np.random.rand(10)}
    spike_rates = {n: a - np.random.normal(size=a.shape)*0.05 for n,a in activations.items()}
    sim_data = {'spike_rates':spike_rates,
                'activations':activations
                }
    layer_data_list = gen_data_net(model, sim_data)
    json.dump(layer_data_list, open('../public/tmp_net_data.json', 'w'))
